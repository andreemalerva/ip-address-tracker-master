let map;
let marker;
function initMap() {
    window.onload = () => {
        document.getElementById("button-addon2").addEventListener("click", e => {
            e.preventDefault();
            validateIp("idIP");
        });
    }
    const validateIp = idElement => {
        const element = document.getElementById(idElement);
        // Patron para validar la IP
        const patronIp = new RegExp(/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gm);
        if (element.value.search(patronIp) == 0) {
            // Ip correcta
            //$('#button-addon2').click(function(e) {
            $.getJSON('https://ipapi.co/' + $('.ip').val() + '/json', function (data) {
                $('#ip').text(data.ip);
                $('#country').text(data.country);
                $('#city').text(data.city);
                //$('#region').text(data.region);
                $('#timezone').text(data.timezone);
                map = new google.maps.Map(document.getElementById("map"), {
                    center: { lat: data.latitude, lng: data.longitude },
                    zoom: 15,
                });
                map.controls[google.maps.ControlPosition.TOP_CENTER].push(
                    document.getElementById("info")
                );
                marker = new google.maps.Marker({
                    map,
                    draggable: true,
                    position: { lat: data.latitude, lng: data.longitude },
                });
            });
            //});
        } else {
            // Ip incorrecta
            alert('Tu direccion IP es incorrecta, por favor revisa tu escritura');
        }
    }
}